git config --global user.name "Cédric Souverain"
git config --global user.email "souverainc@eisti.eu"

git clone git@gitlab.com:souverainc/documentation-gitlab.git
cd documentation-gitlab

touch README.md

git add README.md
git commit -m "add README"
git push -u origin master
